﻿using UnityEngine;
using System.Collections;

public class CloudController : MonoBehaviour {
  public float offset;

  // Use this for initialization
  void Start () {
    GetComponent<Animator>().Play("CloudUpDown", 0, offset);  
  }
  
  // Update is called once per frame
  void Update () {
  
  }
}
