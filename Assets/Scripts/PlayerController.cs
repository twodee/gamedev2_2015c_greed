﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
  public GameObject exploder;

  public Animator animator;
  public float speed;

  public Transform groundSensor;
  public float radius;
  public float jumpForce;
  public LayerMask groundLayers;

  private new Rigidbody2D rigidbody;
  private bool isFacingRight = true;
  private bool isGrounded = true;

  void Start() {
    rigidbody = GetComponent<Rigidbody2D>(); 
  }

  void Update() {
    if (isGrounded && Input.GetButtonDown("Jump")) {
      rigidbody.AddForce(new Vector2(0, jumpForce));
      isGrounded = false;
    }

    animator.SetBool("IsGrounded", isGrounded);

    if (Input.GetButtonDown("Fire1")) {
      exploder.SetActive(true);
    }
  }
  
  void FixedUpdate() {
    float input = Input.GetAxis("Horizontal");
    if (isFacingRight && input < 0 || !isFacingRight && input > 0) {
      Flip();
    }

    rigidbody.velocity = new Vector2(input * speed, rigidbody.velocity.y);
    animator.SetFloat("Speed", Mathf.Abs(rigidbody.velocity.x));

    isGrounded = Physics2D.OverlapCircle(groundSensor.position, radius, groundLayers);
  }

  void Flip()
  {
    isFacingRight = !isFacingRight;
    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
  }

}
