﻿using UnityEngine;
using System.Collections;

public class SpawnerController : MonoBehaviour {

  public GameObject prefabToSpawn;
  public int numberToSpawn;
  public float delay;

	// Use this for initialization
	void Start () {
    StartCoroutine(Spawn());
	}

  IEnumerator Spawn() {
    while (numberToSpawn > 0) {
      Instantiate(prefabToSpawn, transform.position, Quaternion.identity);
      numberToSpawn--;
      yield return new WaitForSeconds(delay);
    }
  }

}
